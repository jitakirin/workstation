#!/bin/bash

#set -euo pipefail

# VARS

# Some systems don't put `*/sbin` directories on PATH for regular users
# as those executables often require root. Unfortunately that prevents
# checks (`which` / `type -p`) for those executables from working
# correctly.
for sbin in /sbin /usr/sbin /usr/local/sbin; do
  if [[ ! ${PATH} =~ (^|:)${sbin}(:|$) ]]; then
    PATH="${PATH}:${sbin}"
  fi
done

source <(sed -re '/^(#.*|)$/d' -e 's/^/OS_/' /etc/os-release)
os_id_like="${OS_ID_LIKE:-${OS_ID}}"

case "${os_id_like}" in
  alpine)
    os_pkg_manager="apk"
    os_packages=(
      # for bork's user type (this is normally pre-installed on most
      # distros)
      shadow
      # for pipsi
      python3
    )
    ;;
  debian)
    os_pkg_manager="apt"
    os_packages=(
      # for pipsi
      python3-venv
    )
    ;;
  suse)
    os_pkg_manager="zypper"
    os_packages=(
      # for pipsi
      python3
    )
    ;;
  *) echo 'Unsupported OS package manager' >&2; return 1 ;;
esac

# Find an existing admin group we can use
#
# Should use 'staff' instead, it's meant for giving users access to
# install stuff into `/usr/local` but doesn't exist by default on many
# distros (e.g. openSUSE).
#
# TODO: find how these groups are created on their respective systems
#   and see if there's a dependency we need to install to have them
adm_groups=( staff adm trusted )
#adm_group=
for grp in "${adm_groups[@]}"; do
  if getent group "${grp}" &>/dev/null; then
    adm_group="${grp}"
    break
  fi
done


pipsi_global_bin_dir="${PIPSI_GLOBAL_BIN_DIR:-"/usr/local/bin"}"
pipsi_global_home="${PIPSI_GLOBAL_HOME:-"/usr/local/lib/pipsi"}"

# ASSERTIONS

for pkg in "${os_packages[@]}"; do
  ok "${os_pkg_manager}" "${pkg}"
done

# Directories and permissions for an admin group to be able to install
# packages via pipsi in global location
# TODO:
ok user "${USER:?}" --groups="${adm_group}"
for pipsi_dir in "${pipsi_global_bin_dir}" "${pipsi_global_home}"; do
  ok directory "${pipsi_dir}" --group="${adm_group}" --mode=2775
done

# Bootstrap pipsi
ok pipsi --global

# Install Ansible
ok pipsi ansible --global
