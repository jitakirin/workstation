#!/bin/sh
# Stage 0
# First thing that runs, so pretty much nothing exists on a machine,
# possibly not even git. Meant to be run as `curl ... | sh`.
# Bootstrap enough to run "bork" as stage 1.

set -eu

pkgs() {
  if command -v apk >/dev/null 2>&1; then
    apk add -U "$@"
  elif command -v apt-get >/dev/null 2>&1; then
    apt-get update -q
    apt-get install -y "$@"
  elif command -v yum >/dev/null 2>&1; then
    yum install "$@"
  elif command -v zypper >/dev/null 2>&1; then
    zypper -qn install "$@"
  else
    echo "Unknown OS package manager" >&2
    return 1
  fi
}

{
  if ! command -v git >/dev/null 2>&1; then
    pkgs git sudo
  fi

  : ${BOOTSTRAP_REPO:=https://gitlab.com/jitakirin/workstation}
  : ${BOOTSTRAP_WORK_DIR:=${HOME}/wrk/${BOOTSTRAP_REPO#https://}}

  printf 'DEBUG: {BOOTSTRAP_REPO: %s, BOOTSTRAP_WORK_DIR: %s}\n' \
    "${BOOTSTRAP_REPO}" "${BOOTSTRAP_WORK_DIR}"
  if test ! -d "${BOOTSTRAP_WORK_DIR}"; then
    mkdir -p "${BOOTSTRAP_WORK_DIR}"
    git clone --recurse-submodules "${BOOTSTRAP_REPO}" "${BOOTSTRAP_WORK_DIR}"
  fi
  cd "${BOOTSTRAP_WORK_DIR}"

  echo 'Handing off to stage 1'
  exec ./stage1.sh
}
