#!/bin/sh
# Stage 1
# We have git and this repo but possibly not much else.
# Bootstrap enough to run Ansible.

set -eu

BOOTSTRAP_WORK_DIR="${0%/*}"

echo "Stage 1: running bork in ${BOOTSTRAP_WORK_DIR}"
"${BOOTSTRAP_WORK_DIR}/vendor/bork/bin/bork" satisfy stage1.bork.sh
