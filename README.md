# workstation

Scripts, hacks and playbooks to bootstrap my workstation(s).

The bootstrap process is divided into stages, numbered from 0 to 2:

- **stage 0**: runs first, assumes nothing is on a machine so it will
  ensure `git` is available, fetch this repo and hand off to stage 1
- **stage 1**: runs from cloned repo, uses [bork][] to bootstrap enough
  to run Ansible
- **stage 2**: runs [Ansible][]

[bork][] is used in stage 1 as it has minimal dependencies (only bash)
and can be run directly after cloning this repo (which includes it as a
git submodule). It is not as powerful as [Ansible][] but much easier
than plain old scripts.

## Getting Started

To fire off the stages from the beginning do:

```sh
curl -fsSL https://is.gd/jiWrkstnBoot | sudo sh
```

This short URL - https://is.gd/jiWrkstnBoot - points to `stage0.sh`
file in this repo, which will ensure *git* is available on the machine,
clone this repo into `${HOME}/wrk/gitlab.com/jitakirin/workstation` dir
and execute `stage1.sh` from there.

## What Does It Do

### stage 0

As mentioned, the short curl command, or `stage0.sh` will ensure *git*
is available and install it if not, it will also install *sudo* which
is necessary for certain [bork][] types. After that, it will fetch this
repo and hand off to `stage1.sh`.

### stage 1

`stage1.sh` uses [bork][] to satisfy assertions specified in
`stage1.bork.sh`. Those in turn will ensure Python 3 is installed on
the machine and it is capable of running `venv`.

Next it will try to find existing group for local users who should be
capable of installing files into `/usr/local` hierarchy, one of
*staff*, *adm* or *trusted*. It will then ensure current user is a
member of that group, and ensure `/usr/local/bin` and
`/usr/local/lib/pipsi` is writable for that group.

Next it will bootstrap [pipsi][] installing it into those directories.

Once pipsi is available, it will use it to install [Ansible][] making
it available as `/usr/local/bin/ansible*`.


[Ansible]: https://www.ansible.com/
[bork]: https://github.com/mattly/bork
[pipsi]: https://github.com/mitsuhiko/pipsi
